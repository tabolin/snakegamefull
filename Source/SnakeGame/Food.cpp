// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			AFood::CreateFoodActor();
			Destroy();
		}
	}
}

void AFood::CreateFoodActor(int ElementsNum)
{

	float MinX = -300.f; float MaX = 300.f;
	float MinY = -300.f; float MaY = 300.f;
	float SpawnZ = 30.f;

	for (int i = 0; i < ElementsNum; ++i)
	{
		float SpawnX = FMath::RandRange(MinX, MaX);
		float SpawnY = FMath::RandRange(MinY, MaY);

		FVector NewLocation(SpawnX, SpawnY, SpawnZ);
		FTransform NewTransform(NewLocation);

		GetWorld()->SpawnActor<AFood>(FoodActorClass, NewTransform);
	}

}